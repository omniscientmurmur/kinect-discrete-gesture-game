How to modify custom gestures:

The .gbd database, currently Database\ShadoWeaving.gbd, controls the gestures that 
are used. See ShadowWeavingGuide.pdf for a description of the custom gestures I'm 
using.

You can make your own gestures too. See:
http://channel9.msdn.com/Blogs/k4wdev/Custom-Gestures-End-to-End-with-Kinect-and-Visual-Gesture-Builder



How to modify gameplay:

If you want to play the main 'dragon fighting game,' right inside of GameState(), 
have StartGame((int)Minigames.Pillage);

If you want to train people to play, use StartGame((int)Minigames.AllAttack);

Game accomodates five players training (it moves on to the next training level 
once five players have sucessfully completed the move). If you want to modify 
this use the variable int num_training = 5;
