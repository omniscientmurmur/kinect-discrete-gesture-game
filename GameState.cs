﻿//------------------------------------------------------------------------------
// <copyright file="GestureResultView.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.DiscreteGestureBasics
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Media;

    /// <summary>
    /// Stores game state.
    /// Properties are stored/updated for display in the UI.
    /// </summary>
    public sealed class GameState : INotifyPropertyChanged
    {
        /// variables for tracking which minigame is in progress
        /// name of minigame level
        public int minigame = 0;

        // a note about this enum: the png files for minigame cutscenes are indexed by the order of this enum, so DONT CHANGE IT
        // AllAttack6, AllShield 7, AllReveal 8, AllBind 9, SequenceRSR 10, SequenceASB 11
        enum Minigames { Pillage, Hide, Volleyball, Drought, Tourist, Battle, AllAttack, AllShield, AllReveal, AllBind, SequenceRSR, SequenceASB };

        /// <summary> Image to show when no attacks take place </summary>
        private readonly ImageSource empty = new BitmapImage(new Uri(@"GameStateImages\empty.png", UriKind.Relative));
        private readonly ImageSource practice = new BitmapImage(new Uri(@"GameStateImages\practice.png", UriKind.Relative));

        // image for the hide game
        private readonly ImageSource visible = new BitmapImage(new Uri(@"GameStateImages\Head2.png", UriKind.Relative));

        /// <summary> Dragon pillage image </summary>
        private readonly ImageSource pillage1 = new BitmapImage(new Uri(@"GameStateImages\dragon5-01.png", UriKind.Relative));
        private readonly ImageSource pillage2 = new BitmapImage(new Uri(@"GameStateImages\dragon7-01.png", UriKind.Relative));

        /// <summary> Images for the drought game </summary>
        private readonly ImageSource drink = new BitmapImage(new Uri(@"GameStateImages\dragon2-01.png", UriKind.Relative));
        private readonly ImageSource choke = new BitmapImage(new Uri(@"GameStateImages\dragon6-01.png", UriKind.Relative));

        /// <summary> User images </summary>
        private readonly ImageSource reveal_weak = new BitmapImage(new Uri(@"GameStateImages\Reveal_weak.png", UriKind.Relative));
        private readonly ImageSource reveal_strong = new BitmapImage(new Uri(@"GameStateImages\Reveal_strong.png", UriKind.Relative));
        private readonly ImageSource bind_weak = new BitmapImage(new Uri(@"GameStateImages\Bind_weak.png", UriKind.Relative));
        private readonly ImageSource bind_strong = new BitmapImage(new Uri(@"GameStateImages\Bind_strong.png", UriKind.Relative));
        private readonly ImageSource shield_weak = new BitmapImage(new Uri(@"GameStateImages\Shield_weak.png", UriKind.Relative));
        private readonly ImageSource shield_strong = new BitmapImage(new Uri(@"GameStateImages\Shield_strong.png", UriKind.Relative));
        private readonly ImageSource attack_weak = new BitmapImage(new Uri(@"GameStateImages\Attack_weak.png", UriKind.Relative));
        private readonly ImageSource attack_strong = new BitmapImage(new Uri(@"GameStateImages\Attack_strong.png", UriKind.Relative));

        // images for volleyball level
        private readonly ImageSource volleyball0 = new BitmapImage(new Uri(@"GameStateImages\cart6-01.png", UriKind.Relative));
        private readonly ImageSource volleyball1 = new BitmapImage(new Uri(@"GameStateImages\cart4-01.png", UriKind.Relative));
        private readonly ImageSource volleyball2 = new BitmapImage(new Uri(@"GameStateImages\cart2-01.png", UriKind.Relative));
        private readonly ImageSource volleyball3 = new BitmapImage(new Uri(@"GameStateImages\cart0-01.png", UriKind.Relative));

        // images for tourist
        private readonly ImageSource holdingTourist = new BitmapImage(new Uri(@"GameStateImages\dragon4-01.png", UriKind.Relative));
        private readonly ImageSource droppingTourist = new BitmapImage(new Uri(@"GameStateImages\dropping_tourist.png", UriKind.Relative));

        // images for battle game
        private readonly ImageSource neutral = new BitmapImage(new Uri(@"GameStateImages\dragon7-01.png", UriKind.Relative));
        private readonly ImageSource attack_prep = new BitmapImage(new Uri(@"GameStateImages\dragon1-01.png", UriKind.Relative));
        private readonly ImageSource vulnerable = new BitmapImage(new Uri(@"GameStateImages\dragon3-01.png", UriKind.Relative));

        /// Now for defualt images for the different layers

        /// <summary> Layer 0 is sky and back buildings </summary>
        private readonly ImageSource imageLayer0 = new BitmapImage(new Uri(@"GameStateImages\background-01.png", UriKind.Relative));

        /// <summary> Layer 1 is behind millikan </summary>
        private ImageSource imageLayer1 = new BitmapImage(new Uri(@"GameStateImages\empty.png", UriKind.Relative));

        /// <summary> Layer 2 is millikan and ground </summary>
        private readonly ImageSource imageLayer2 = new BitmapImage(new Uri(@"GameStateImages\millikan-01.png", UriKind.Relative));

        /// <summary> Layer 3 is behind trees but in front of millikan </summary>
        private ImageSource imageLayer3 = new BitmapImage(new Uri(@"GameStateImages\empty.png", UriKind.Relative));

        /// <summary> Layer 4 is trees </summary>
        private readonly ImageSource imageLayer4 = new BitmapImage(new Uri(@"GameStateImages\front_trees-01.png", UriKind.Relative));

        /// <summary> Layer 5 is in front of trees </summary>
        private ImageSource imageLayer5 = new BitmapImage(new Uri(@"GameStateImages\empty.png", UriKind.Relative));

        /// <summary> Layer 6 is player effects </summary>
        private ImageSource imageLayer6 = new BitmapImage(new Uri(@"GameStateImages\empty.png", UriKind.Relative));

        /// <summary> Layer 7 are cutscene images </summary>
        private ImageSource imageLayer7 = new BitmapImage(new Uri(@"GameStateImages\empty.png", UriKind.Relative));

        /// Cutscene related variables
        /// Cutscenes occurr between minigames and provide text descriptions
        // cutscene path
        private string cutscenePath = @"CutsceneImages\";

        enum Cutscenes { Begin, Win, Lose };

        // true when a cutscene is in progress
        private bool cutsceneFlag = false;

        // length of the cutscene in seconds
        private int cutsceneLength = 5;

        // one cutscene can be queued
        private int pendingCutscene = -1;


        /// Variables related to the state of the dragon
        // dragon's life points
        private double hitPoints;

        // name of hit points for this game
        private string dragonBarName = " ";

        /// the state the dragon is in during this level of the game
        private int D = 0;

        /// probability that the dragon changes state each update
        private double dragon_change_state_prob = 0.03;


        /// Variables related to the state of the player
        // Array mapping players to their current stance </summary>
        private int[] player_stance = null;

        //For each shadow weaving gesture, how many players are performing that gesture? </summary>
        private int[] gesture_instances = null;

        // Numbered spells. access as "(int) Spells.Shield"
        enum Spells { Shield, Attack, Bind, Reveal };

        // need to be able to communicate with all the player objects
        private readonly GestureResultView [] players = null;


        //// Variables for timing things like the cutscenes etc.
        // time at which the last timer event was started
        private double timestamp;

        // time since timestamp, the elapsed time of the most recent timer
        private double time_elapsed = 0;

        // how many people need to complete the training sequence
        private int num_training = 5;

        /// <summary>
        /// Initializes a new instance of the GameState class and sets initial property values
        /// </summary>
        public GameState()
        {
            //StartGame((int) Minigames.Pillage);
            StartGame((int)Minigames.Pillage);

            // every player begins in the neutral gesture state
            this.player_stance = new int[6];
            for (int i = 0; i < 6; i++)
            {
                this.player_stance[i] = -1;
            }

            // no gestures are presently being performed
            this.gesture_instances = new int[4];
            for (int i = 0; i < 4; i++)
            {
                this.gesture_instances[i] = 0;
            }

            this.players = new GestureResultView[6];
        }
        // Begin a cutscene by loading the file and setting the timer
        private void beginCutscene(int cutsceneType)
        {
            if (this.cutsceneFlag)
            {
                // cutscene is already playing so this one needs to be queued
                this.pendingCutscene = cutsceneType;
            }
            else
            {
                // load the cutscene image from file and display it on the top layer
                string image_location = string.Format(@"{0}_{1}.png", this.minigame, cutsceneType);
                image_location = cutscenePath + image_location;
                try
                {
                    ImageSource cut_i = new BitmapImage(new Uri(image_location, UriKind.Relative));
                    ImageLayer7 = cut_i;
                }
                catch 
                {
                    // FileNotFoundExceptions are handled here.
                    Console.WriteLine("ERROR: Cutscene file not found. Expected path {0}", image_location);
                    ImageLayer7 = this.empty;
                }

                // set a timer so that the image can be removed later
                this.cutsceneFlag = true;
                StartTimer();
            }
        }

        /// Update this.time_elapsed
        /// 
        public void UpdateElapsedTime()
        {
            TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            double now = span.TotalSeconds;
            this.time_elapsed = now - this.timestamp;
        }

        // start timer
        public void StartTimer()
        {
            TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            this.timestamp = span.TotalSeconds;
        }

        // Main Window is gonna hook you up with references to all the GestureResultView objects
        public void linkPlayer(int i, GestureResultView r){
            players[i] = r;
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///  there are three layers in which the dragon may be present. Since the dragon can only be present 
        ///  in one of these, use this function to remove any stray dragons on other layers
        /// </summary>
        private void updateDragonImage(int imageLayer, ImageSource I)
        {
            if (imageLayer == 1)
            {
                this.ImageLayer1 = I;
                this.ImageLayer3 = this.empty;
                this.ImageLayer5 = this.empty;
            }
            else if (imageLayer == 3)
            {
                this.ImageLayer1 = this.empty;
                this.ImageLayer3 = I;
                this.ImageLayer5 = this.empty;
            }
            else if (imageLayer == 5)
            {
                this.ImageLayer1 = this.empty;
                this.ImageLayer3 = this.empty;
                this.ImageLayer5 = I;
            }
        }

        /// Update ImageSource given player_stance array and the dragon's state
        public void selectImage()
        {
            if (this.cutsceneFlag)
            {
                // a cutscene is currently displayed. Decide if you're going to end it
                UpdateElapsedTime();
                if (this.time_elapsed > this.cutsceneLength)
                {
                    ImageLayer7 = this.empty;
                    this.cutsceneFlag = false;

                    // if we just finished a cutscene we want to know if a new one needs to begin because it has been queued
                    if (this.pendingCutscene != -1)
                    {
                        beginCutscene(this.pendingCutscene);
                        this.pendingCutscene = -1;
                    }
                }
            }

            if (this.minigame == (int)Minigames.Pillage)
            {                
                // for this game, player state is determined by the number of players doing an attack
                int attacks = this.gesture_instances[(int)Spells.Attack];

                // for this game there are three user states, U
                //0 - user neutral
                //1 - user attacking weak
                //2 - user attacking strong
                if (attacks == 0)
                {
                    ImageLayer6 = this.empty;
                }
                else if (attacks == 1)
                {
                    ImageLayer6 = this.attack_weak;
                }
                else if (attacks > 1)
                {
                    ImageLayer6 = this.attack_strong;
                }

                // now for dragon images
                if (this.D == 0)
                {
                    updateDragonImage(3, this.pillage1);
                }
                else
                {
                    updateDragonImage(3, this.pillage2);
                }
            }
            else if (this.minigame == (int)Minigames.Hide)
            {
                // for this game, player state is determined by the number of players doing a reveal
                int reveals = this.gesture_instances[(int)Spells.Reveal];

                // for this game there are three user states, U
                // 0 - no users revealing
                // 1 - one user revealing
                // 2 - more than one user revealing
                if (reveals == 0)
                {
                    ImageLayer6 = this.empty;
                }
                else if (reveals == 1)
                {
                    ImageLayer6 = this.reveal_weak;
                }
                else if (reveals > 1)
                {
                    ImageLayer6 = this.reveal_strong;
                }

                // now for dragon images
                if (this.D == 1)
                {
                    updateDragonImage(1, this.visible);
                }
                else
                {
                    updateDragonImage(1, this.empty);
                }

            }
            else if (this.minigame == (int)Minigames.Volleyball)
            {
                // for this game, player state is determined by the number of players doing a shielding
                int shields = this.gesture_instances[(int)Spells.Shield];

                // for this game there are three user states, U
                // 0 - no users shielding
                // 1 - one user shielding
                // 2 - more than one user shielding
                if (shields == 0)
                {
                    ImageLayer6 = this.empty;
                }
                else if (shields == 1)
                {
                    ImageLayer6 = this.shield_weak;
                }
                else if (shields > 1)
                {
                    ImageLayer6 = this.shield_strong;
                }

                // now for dragon images
                if (this.D == 0)
                {
                    updateDragonImage(5, this.volleyball0);
                }
                else if (this.D == 1)
                {
                    updateDragonImage(5, this.volleyball1);
                }
                else if (this.D == 2)
                {
                    updateDragonImage(5, this.volleyball2);
                }
                else if (this.D == 3)
                {
                    updateDragonImage(5, this.volleyball3);
                }
            }
            else if (this.minigame == (int)Minigames.Drought)
            {                
                // for this game, player state is determined by the number of players binding
                int binding = this.gesture_instances[(int)Spells.Bind];

                // for this game there are three user states, U
                // 0 - no users binding
                // 1 - one user binding
                // 2 - more than one user binding
                if (binding == 0)
                {
                    ImageLayer6 = this.empty;
                }
                else if (binding == 1)
                {
                    ImageLayer6 = this.bind_weak;
                }
                else if (binding > 1)
                {
                    ImageLayer6 = this.bind_strong;
                }

                // now for dragon images
                if (this.D == 0)
                {
                    updateDragonImage(3, this.drink);
                }
                else
                {
                    updateDragonImage(3, this.choke);
                }

            }
            else if (this.minigame == (int)Minigames.Tourist)
            {
                // for this game, player state is determined by the number of players doing an attack
                int attacks = this.gesture_instances[(int)Spells.Attack];

                // for this game there are three user states, U
                //0 - user neutral
                //1 - user attacking weak
                //2 - user attacking strong
                if (attacks == 0)
                {
                    ImageLayer6 = this.empty;
                }
                else if (attacks == 1)
                {
                    ImageLayer6 = this.attack_weak;
                }
                else if (attacks > 1)
                {
                    ImageLayer6 = this.attack_strong;
                }

                // now for dragon images
                if (this.D == 0)
                {
                    updateDragonImage(3, this.holdingTourist);
                }
                else
                {
                    updateDragonImage(5, this.droppingTourist);
                }
            }
            else if (this.minigame == (int)Minigames.Battle)
            {
                // for this game, player state is determined by the number of players doing an attack
                int attacks = this.gesture_instances[(int)Spells.Attack];

                // for this game there are three user states, U
                //0 - user neutral
                //1 - user attacking weak
                //2 - user attacking strong
                if (attacks == 0)
                {
                    ImageLayer6 = this.empty;
                }
                else if (attacks == 1)
                {
                    ImageLayer6 = this.attack_weak;
                }
                else if (attacks > 1)
                {
                    ImageLayer6 = this.attack_strong;
                }

                // now for dragon images
                if (this.D == 0)
                {
                    updateDragonImage(3, this.neutral);
                }
                else if (this.D == 1)
                {
                    updateDragonImage(3, this.attack_prep);
                }
                else if (this.D == 2)
                {
                    updateDragonImage(5, this.pillage1);
                }
                else if (this.D == 3)
                {
                    updateDragonImage(5, this.vulnerable);
                }
            }
            else if (this.minigame == (int)Minigames.AllAttack
                  || this.minigame == (int)Minigames.AllShield
                  || this.minigame == (int)Minigames.AllBind
                  || this.minigame == (int)Minigames.AllReveal
                  || this.minigame == (int)Minigames.SequenceRSR
                  || this.minigame == (int)Minigames.SequenceASB)
            {
                updateDragonImage(5, this.practice);
            }

            else
            {
                Console.WriteLine("Error, trying to make images of minigame {0}", minigame);
            }
        }

        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageLayer0{
            get{
                return this.imageLayer0;
            }

            private set{
                if (this.imageLayer0 != value){
                    Console.WriteLine("Bad trying to assing to readonly sky and buildings layer");
                }
            }
        }

        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageLayer1
        {
            get
            {
                return this.imageLayer1;
            }

            private set
            {
                if (this.imageLayer1 != value)
                {
                    this.imageLayer1 = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageLayer2
        {
            get
            {
                return this.imageLayer2;
            }

            private set
            {
                if (this.imageLayer2 != value)
                {
                    Console.WriteLine("Bad trying to assign to readonly millikan and ground layer");
                }
            }
        }
        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageLayer3
        {
            get
            {
                return this.imageLayer3;
            }

            private set
            {
                if (this.imageLayer3 != value)
                {
                    this.imageLayer3 = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageLayer4
        {
            get
            {
                return this.imageLayer4;
            }

            private set
            {
                if (this.imageLayer4 != value)
                {
                    Console.WriteLine("Bad trying to assign to readonly trees layer");
                }
            }
        }
        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageLayer5
        {
            get
            {
                return this.imageLayer5;
            }

            private set
            {
                if (this.imageLayer5 != value)
                {
                    this.imageLayer5 = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageLayer6
        {
            get
            {
                return this.imageLayer6;
            }

            private set
            {
                if (this.imageLayer6 != value)
                {
                    this.imageLayer6 = value;
                    this.NotifyPropertyChanged();
                }
            }
        }        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageLayer7
        {
            get
            {
                return this.imageLayer7;
            }

            private set
            {
                if (this.imageLayer7 != value)
                {
                    this.imageLayer7 = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the current hit points
        /// </summary>
        public double HitPoints
        {
            get
            {
                return this.hitPoints;
            }

            set
            {
                if (this.hitPoints != value)
                {
                    this.hitPoints = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the current dragonBarName text to display
        /// </summary>
        public string DragonBarName
        {
            get
            {
                return this.dragonBarName;
            }

            set
            {
                if (this.dragonBarName != value)
                {
                    this.dragonBarName = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("dragonBarName"));
                    }
                }
            }
        }

        // Do any setup for the new game
        public void StartGame(int minigame)
        {
            Console.WriteLine("{0} is the new minigame", minigame);
            this.minigame = minigame;
            if (minigame == (int)Minigames.Pillage)
            {
                this.DragonBarName = "Dragon is focused on pillaging";
                this.HitPoints = 100;
                this.D = 0;
                beginCutscene((int) Cutscenes.Begin);
            }
            else if (minigame == (int)Minigames.Hide)
            {
                this.DragonBarName = "Dragon is happy hiding";
                this.HitPoints = 100;
                this.D = 1;
            }
            else if (minigame == (int)Minigames.Volleyball)
            {
                this.DragonBarName = "The dragon wants to play volleyball!";
                this.HitPoints = 100;
                this.D = 0;
                StartTimer();
            }
            else if (minigame == (int)Minigames.Drought)
            {
                this.DragonBarName = "Stack's water quota";
                this.HitPoints = 0;
                this.D = 0;
                StartTimer();
            }
            else if (minigame == (int)Minigames.Tourist)
            {
                Console.WriteLine("Resetting tourist");
                this.DragonBarName = "Dragon luuuuves her tourist!";
                this.HitPoints = 100;
                this.D = 0;
            }
            else if (minigame == (int)Minigames.Battle)
            {
                this.DragonBarName = "Healthy dragon is angry!";
                this.HitPoints = 100;
                this.D = 0;
                StartTimer();
            }
            else if (minigame == (int)Minigames.AllAttack)
            {
                this.DragonBarName = "Everyone Practice Attack!";
                this.HitPoints = 0;
            }
            else if (minigame == (int)Minigames.AllShield)
            {
                this.DragonBarName = "Everyone Practice Shield!";
                this.HitPoints = 0;
            }
            else if (minigame == (int)Minigames.AllBind)
            {
                this.DragonBarName = "Everyone Practice Bind!";
                this.HitPoints = 0;
            }
            else if (minigame == (int)Minigames.AllReveal)
            {
                this.DragonBarName = "Everyone Practice Reveal!";
                this.HitPoints = 0;
            }
            else if (minigame == (int)Minigames.SequenceRSR)
            {
                this.DragonBarName = "Everyone Practice Sequence Reveal-Shield-Reveal!";
                this.HitPoints = 0;
            }
            else if (minigame == (int)Minigames.SequenceASB)
            {
                this.DragonBarName = "Everyone Practice Sequence Attack-Shield-Bind!";
                this.HitPoints = 0;
            }
            else
            {
                Console.WriteLine("Error, trying to start bad minigame number {0}", minigame);
            }

            beginCutscene((int)Cutscenes.Begin);
        }

        // Execute minigame failure
        public void failMinigame()
        {
            beginCutscene((int)Cutscenes.Lose);

            // reset health
            for (int j = 0; j < 6; j++)
            {
                this.players[j].ChangePlayerHealth(100);
                this.players[j].ChangePlayerMagic(100);
            }

            // restart level that you failed
            this.StartGame(this.minigame);
        }

        /// Even if no one is shadow weaving, this function takes care of game mechanics that need to occur on each timestep
        public void eachFrameGameMechanics()
        {
            if (this.cutsceneFlag)
            {
                // don't do anything. we don't update dragon during cutscene
            }
            else if (this.minigame == (int)Minigames.Pillage)
            {
                // decide if the dragon state changes
                Random rnd = new Random();
                double number = rnd.NextDouble();
                if (number < this.dragon_change_state_prob)
                {
                    if (this.D == 1)
                    {
                        this.D = 0;
                    }
                    else
                    {
                        this.D = 1;
                    }
                    selectImage();
                }
            }
            else if (this.minigame == (int)Minigames.Hide)
            {
                // decide if the dragon state changes
                Random rnd = new Random();
                double number = rnd.NextDouble();
                if (number < this.dragon_change_state_prob)
                {
                    if (this.D == 1)
                    {
                        this.D = 0;
                    }
                    else
                    {
                        this.D = 1;
                    }
                    selectImage();
                }
            }
            else if (this.minigame == (int)Minigames.Volleyball)
            {
                // decide if the volleyball state changes
                UpdateElapsedTime();
                // ball moves every 2 seconds
                if (this.time_elapsed > 2.0)
                {
                    this.D += 1;
                    selectImage();
                    StartTimer();
                }

                // If volleyball enters state n+2, players lose hit points
                if (this.D >= 4)
                {
                    Console.WriteLine("Hit by ball");
                    for (int i = 0; i < 6; i++)
                    {
                        this.players[i].ChangePlayerHealth(-50);
                    }
                    this.D = 0;
                    selectImage();
                }
            }
            else if (this.minigame == (int)Minigames.Drought)
            {
                UpdateElapsedTime();
                // Lose: dragon has drunk all the water
                if (this.hitPoints >= 99)
                {
                    Console.WriteLine("Losing drought minigame");
                    failMinigame();
                }

                // Win: dragon hasn't drunk all the water
                else if (this.time_elapsed > 30)
                {
                    Console.WriteLine("Winning drought minigame");

                    beginCutscene((int)Cutscenes.Win);
                    StartGame((int)Minigames.Tourist);
                }
                selectImage();
            }
            else if (this.minigame == (int)Minigames.Battle)
            {
                // decide if the volleyball state changes
                UpdateElapsedTime();
                // ball moves every 2 seconds
                if (this.time_elapsed > 2.0)
                {
                    this.D++;
                    selectImage();
                    StartTimer();
                    if (this.D >= 4)
                    {
                        this.D = 0;
                    }
                }
            }
            else if (this.minigame == (int)Minigames.Tourist)
            {
                if (this.D == 1)
                {
                    this.HitPoints = this.hitPoints - 1;
                }
            }
        }

        /// Game mechanics
        /// process changes to the game state that depend on a specific player changing their stance
        public void gameMechanics()
        {
            if (this.minigame == (int)Minigames.Pillage)
            {
                //When user:1 and dragon:0, dragon decreases a few hit points
                if (gesture_instances[(int)Spells.Attack] == 1 && this.D == 0)
                {
                    this.HitPoints = this.HitPoints - 3;
                }

                //When user:2 and dragon:0, dragon decreases many hit points                
                if (gesture_instances[(int)Spells.Attack] >= 2 && this.D == 0)
                {
                    this.HitPoints = this.HitPoints - 10;
                }

                // When the dragon has no hit points, players win. 
                if (this.HitPoints <= 0)
                {
                    beginCutscene((int)Cutscenes.Win);
                    StartGame((int)Minigames.Hide);
                }
            }
            else if (this.minigame == (int)Minigames.Hide)
            {
                // When players enter state 2 (reveal strong) while the dragon is in state 1 (partially revealed), the dragon loses hit points.
                if (gesture_instances[(int)Spells.Reveal] >= 2 && this.D == 1)
                {
                    this.HitPoints = this.HitPoints - 10;
                }

                // When the dragon has no hit points, players win. 
                if (this.HitPoints <= 0)
                {
                    beginCutscene((int)Cutscenes.Win);
                    StartGame((int)Minigames.Volleyball);
                }
            }
            else if (this.minigame == (int)Minigames.Volleyball)
            {
                // When players enter state 2 (user shield strong) while dragon (volleyball) is in state n+1, the volleyball bounces, and dragon loses hit points.
                if (gesture_instances[(int)Spells.Shield] >= 2 && this.D == 3)
                {
                    this.HitPoints = this.HitPoints - 10;
                    Console.WriteLine("D going back to state 1");
                    this.D = 0; // volleyball bounces
                }
           
                if (this.HitPoints <= 0)
                {
                    beginCutscene((int)Cutscenes.Win);
                    StartGame((int)Minigames.Drought);
                }
            }
            else if (this.minigame == (int)Minigames.Drought)
            {
                // if enough players are binding then the dragon is in state 1, choking, rathern than state 0, drinking
                if (gesture_instances[(int)Spells.Bind] >= 2)
                {
                    // dragon choking
                    this.D = 1;
                }
                else
                {
                    // don't have the game running during the cutscene
                    if (this.cutsceneFlag != true)
                    {
                        // dragon drinking
                        this.D = 0;

                        // dragon drinks less if more stackers casts bind.
                        if (gesture_instances[(int)Spells.Bind] == 1)
                        {
                            this.HitPoints += 0.1;
                        }
                        else if (gesture_instances[(int)Spells.Bind] == 0)
                        {
                            this.HitPoints += 0.2;
                        }
                    }
                }
            }
            else if (this.minigame == (int)Minigames.Tourist)
            {
                // If each stacker casts attack separately, the tourist is released.
                if (gesture_instances[(int)Spells.Attack] == 1 && this.D == 0)
                {
                    this.HitPoints = this.HitPoints - 2;
                }

                // If more than one player casts attack, the tourist is incinerated.              
                if (gesture_instances[(int)Spells.Attack] >= 2 && this.D == 0)
                {
                    this.D = 1;
                    this.HitPoints = 5;
                }

                // When the dragon has no hit points, players win. 
                if (this.HitPoints <= 0)
                {
                    if (this.D == 1)
                    {
                        failMinigame();
                    }
                    else
                    {
                        beginCutscene((int)Cutscenes.Win);
                        StartGame((int)Minigames.Battle);
                    }
                }

                //playSound("ScreamingTourist1.wav");
            }
            else if (this.minigame == (int)Minigames.Battle)
            {      
                //When user:2 and dragon:3, dragon decreases many hit points                
                if (gesture_instances[(int)Spells.Attack] >= 2 && this.D == 3)
                {
                    this.HitPoints = this.HitPoints - 15;
                }

                // When the dragon has no hit points, players win. 
                if (this.HitPoints <= 0)
                {
                    beginCutscene((int)Cutscenes.Win);
                }

                // If dragon is attacking and players are not shielding, then they lose points
                if (gesture_instances[(int)Spells.Shield] < 2 && this.D == 2)
                {
                    for (int i = 0; i < 6; i++)
                    {
                        this.players[i].ChangePlayerHealth(-1);
                    }
                }
            }
            else if (this.minigame == (int)Minigames.AllAttack
                  || this.minigame == (int)Minigames.AllShield
                  || this.minigame == (int)Minigames.AllBind
                  || this.minigame == (int)Minigames.AllReveal)
            {
                // figure out which pattern we want all the players to do
                // all patterns of length 1
                int[] pattern = new int[1];
                if (this.minigame == (int)Minigames.AllAttack)
                {
                    int[] mypattern = { (int)Spells.Attack };
                    Array.Copy(mypattern, pattern, mypattern.Length);
                }
                else if (this.minigame == (int)Minigames.AllShield)
                {
                    int[] mypattern = { (int)Spells.Shield };
                    Array.Copy(mypattern, pattern, mypattern.Length);
                }
                else if (this.minigame == (int)Minigames.AllBind)
                {
                    int[] mypattern = { (int)Spells.Bind };
                    Array.Copy(mypattern, pattern, mypattern.Length);
                }
                else if (this.minigame == (int)Minigames.AllReveal)
                {
                    int[] mypattern = { (int)Spells.Reveal };
                    Array.Copy(mypattern, pattern, mypattern.Length);
                }

                int playersFinished = 0;
                for (int i = 0; i < 6; i++)
                {
                    if (players[i].spellHistoryMatchArray(pattern))
                    {
                        players[i].setGoldStar(true);
                        playersFinished++;
                    }
                    else
                    {
                        players[i].setGoldStar(false);
                    }
                }

                if (this.minigame == (int)Minigames.AllAttack)
                {
                    this.DragonBarName = string.Format("{0}/5 players have attacked!", playersFinished);
                }
                else if (this.minigame == (int)Minigames.AllShield)
                {
                    this.DragonBarName = string.Format("{0}/5 players have shielded!", playersFinished);
                }
                else if (this.minigame == (int)Minigames.AllBind)
                {
                    this.DragonBarName = string.Format("{0}/5 players have bound!", playersFinished);
                }
                else if (this.minigame == (int)Minigames.AllReveal)
                {
                    this.DragonBarName = string.Format("{0}/5 players have revealed!", playersFinished);
                }

                if (playersFinished >= this.num_training)
                {
                    beginCutscene((int)Cutscenes.Win);
                    //StartGame(this.minigame + 1);

                    // enum changed?
                    if (this.minigame == (int)Minigames.AllAttack)
                    {
                        StartGame((int) Minigames.AllShield);
                    }
                    else if (this.minigame == (int)Minigames.AllShield)
                    {
                        StartGame((int)Minigames.AllBind);
                    }
                    else if (this.minigame == (int)Minigames.AllBind)
                    {
                        StartGame((int)Minigames.AllReveal);
                    }
                    else if (this.minigame == (int)Minigames.AllReveal)
                    {
                        StartGame((int)Minigames.SequenceRSR);
                    }

                    // turn off gold stars
                    for (int i = 0; i < 6; i++)
                    {
                        this.players[i].setGoldStar(false);
                    }                
                }
            }
            else if (this.minigame == (int)Minigames.SequenceRSR
                  || this.minigame == (int)Minigames.SequenceASB)
            {
                // figure out which pattern we want all the players to do
                // these patterns are exactly 3
                int[] pattern = new int[3];
                if (this.minigame == (int)Minigames.SequenceRSR)
                {
                    int[] mypattern = { (int)Spells.Reveal, (int)Spells.Shield, (int)Spells.Reveal };
                    Array.Copy(mypattern, pattern, mypattern.Length);
                }
                else if (this.minigame == (int)Minigames.SequenceASB)
                {
                    int[] mypattern = { (int)Spells.Attack, (int)Spells.Shield, (int)Spells.Bind };
                    Array.Copy(mypattern, pattern, mypattern.Length);
                }

                int playersFinished = 0;
                for (int i = 0; i < 6; i++)
                {
                    if (players[i].spellHistoryMatchArray(pattern))
                    {
                        players[i].setGoldStar(true);
                        playersFinished++;
                    }
                    else
                    {
                        players[i].setGoldStar(false);
                    }
                }

                if (this.minigame == (int)Minigames.SequenceRSR)
                {
                    this.DragonBarName = string.Format("{0}/5 players have Reveal-Shield-Revealed!", playersFinished);
                }
                else if (this.minigame == (int)Minigames.SequenceASB)
                {
                    this.DragonBarName = string.Format("{0}/5 players have Attack-Shield-Bound!", playersFinished);
                }

                if (playersFinished >= this.num_training)  
                {
                    beginCutscene((int)Cutscenes.Win);

                    if (this.minigame == (int)Minigames.SequenceASB)
                    {
                        Console.WriteLine("Finished training");
                        //StartGame((int)Minigames.Pillage);
                    }
                    else
                    {
                        StartGame(this.minigame + 1);
                    }

                    // turn off gold stars
                    for (int i = 0; i < 6; i++)
                    {
                        this.players[i].setGoldStar(false);
                    }
                       
                }
            }
        }

        /// <summary>
        /// Called when a player changes their gesture state
        /// This function is responsible for updating player_stance and gesture_instances
        /// as well as any reaction (loss of hit points) the dragon has to the new gesture
        /// </summary>
        public void playerChangeGesture(int bodyIndex, int newGestureNum)
        {
            int oldGestureNum = this.player_stance[bodyIndex];
            player_stance[bodyIndex] = newGestureNum;

            // record this player adding to the instance of this gesture
            if (newGestureNum >= 0)
            {
                this.gesture_instances[newGestureNum]++;
            }

            // if the player was previously executing a gesture, we need to decrease 
            // the number of players counted as performing that gesture
            if (oldGestureNum >= 0)
            {
                this.gesture_instances[oldGestureNum]--;
            }
        }

        /// <summary>
        /// Called when a gesture has started for any player
        /// </summary>
        public void gestureBegins(int gesture_num, int bodyIndex)
        {
            //eachFrameGameMechanics();
            playerChangeGesture(bodyIndex, gesture_num);
            gameMechanics();
            selectImage();
        }

        /// <summary>
        /// Called when a gesture has ended for any player
        /// </summary>
        public void gestureEnds(int bodyIndex)
        {
            //eachFrameGameMechanics();
            playerChangeGesture(bodyIndex, -1);
            gameMechanics();
            selectImage();
        }

        /// <summary>
        /// Notifies UI that a property has changed
        /// </summary>
        /// <param name="propertyName">Name of property that has changed</param> 
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
