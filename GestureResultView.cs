﻿//------------------------------------------------------------------------------
// <copyright file="GestureResultView.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.DiscreteGestureBasics
{
    using System;
    using System.Collections;   // for queues
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Stores discrete gesture results for the GestureDetector.
    /// Properties are stored/updated for display in the UI.
    /// </summary>
    public sealed class GestureResultView : INotifyPropertyChanged
    {
        // gets its own kinect body view, just for the person its tracking
        /// <summary> KinectBodyView object which handles drawing the Kinect bodies to a View box in the UI </summary>
        public KinectBodyView kinectBodyView = null;

        /// <summary> Image to show when the 'detected' property is true for a tracked body, and it is defending </summary>
        private readonly ImageSource playerShieldImage = new BitmapImage(new Uri(@"Images\PlayerShield.JPG", UriKind.Relative));

        /// <summary> Image to show when the 'detected' property is true for a tracked body, and its attacking </summary>
        private readonly ImageSource playerAttackImage = new BitmapImage(new Uri(@"Images\PlayerAttack.JPG", UriKind.Relative));

        /// <summary> Image to show when the 'detected' property is true for a tracked body, and its attacking </summary>
        private readonly ImageSource playerBindImage = new BitmapImage(new Uri(@"Images\PlayerBind.JPG", UriKind.Relative));

        /// <summary> Image to show when the 'detected' property is true for a tracked body, and its attacking </summary>
        private readonly ImageSource playerRevealImage = new BitmapImage(new Uri(@"Images\PlayerReveal.JPG", UriKind.Relative));

        /// <summary> Image to show when the 'detected' property is false for a tracked body </summary>
        private readonly ImageSource playerNeutralStanceImage = new BitmapImage(new Uri(@"Images\PlayerNeutralStance.JPG", UriKind.Relative));

        /// <summary> Image to show when the body associated with the GestureResultView object is not being tracked </summary>
        private readonly ImageSource playerNotTrackedImage = new BitmapImage(new Uri(@"Images\NotTracked.png", UriKind.Relative));

        /// gold star
        private readonly ImageSource starImage = new BitmapImage(new Uri(@"Images\Star.png", UriKind.Relative));

        /// <summary> Array of brush colors to use for a tracked body; array position corresponds to the body's current gesture </summary>
        //SolidColorBrush brush1 = new SolidColorBrush(Color.FromArgb(255, 255, 139, 0));
        //private readonly Brush[] trackedColors = new Brush[] { brush1, Brushes.Red, Brushes.Green, Brushes.Yellow };

        /// <summary>
        /// Which gesture has been detected?
        /// 0 - shield
        /// 1 - attack
        /// 2 - bind
        /// 3 - reveal
        enum Spells { Shield, Attack, Bind, Reveal };

        // contains the last
        private Stack spellHistory;

        /// </summary>
        private int whichGesture = -1;

        /// <summary> Brush color to use as background in the UI </summary>
        private Brush bodyColor = Brushes.Gray;

        /// <summary> The body index (0-5) associated with the current gesture detector </summary>
        private int bodyIndex = 0;

        /// <summary> The player's current hit points </summary>
        private double hitPoints = 100;

        /// <summary> The player's current magic points </summary>
        private double magicPoints = 100;

        /// <summary> On each update, how many hitpoints are healed? </summary>
        private double hitPointRecovery = 0.2;

        /// <summary> On each update, how many magic points are healed? </summary>
        private double magicPointRecovery = 0.2;

        /// <summary>
        ///  cost in magic points of each of the shadow weaving gestures
        /// </summary>
        private int[] magicCosts = { 40, 30, 20, 10};

        /// <summary> Current confidence value reported by the discrete gesture </summary>
        private float confidence = 0.0f;

        /// <summary> True, if the discrete gesture is currently being detected </summary>
        private bool detected = false;

        /// <summary> Image to display in UI which corresponds to tracking/detection state </summary>
        private ImageSource imageSource = null;
        
        /// <summary> True, if the body is currently being tracked </summary>
        private bool isTracked = false;

        private readonly GameState state;

        // players have a gold star when they've sucessfully woven
        private bool goldStar = false;

        /// <summary>
        /// Initializes a new instance of the GestureResultView class and sets initial property values
        /// </summary>
        /// <param name="bodyIndex">Body Index associated with the current gesture detector</param>
        /// <param name="isTracked">True, if the body is currently tracked</param>
        /// <param name="detected">True, if the gesture is currently detected for the associated body</param>
        /// <param name="confidence">Confidence value for detection of the gesture</param>
        public GestureResultView(int bodyIndex, bool isTracked, bool detected, float confidence, GameState state, KinectBodyView k)
        {
            this.BodyIndex = bodyIndex;
            this.IsTracked = isTracked;
            this.Detected = detected;
            this.Confidence = confidence;
            this.ImageSource = this.playerNotTrackedImage;
            this.state = state;
            this.kinectBodyView = k;
            this.spellHistory = new Stack();
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        // set private star image
        public void setGoldStar(bool value)
        {
            this.goldStar = value;
            this.ImageSource = this.starImage;
            //this.NotifyPropertyChanged();
        }

        /// <summary> 
        /// Gets the body index associated with the current gesture detector result 
        /// </summary>
        public int BodyIndex
        {
            get
            {
                return this.bodyIndex;
            }

            private set
            {
                if (this.bodyIndex != value)
                {
                    this.bodyIndex = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> 
        /// Gets the body color corresponding to the body index for the result
        /// </summary>
        public Brush BodyColor
        {
            get
            {
                return this.bodyColor;
            }

            private set
            {
                if (this.bodyColor != value)
                {
                    this.bodyColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> 
        /// Gets a value indicating whether or not the body associated with the gesture detector is currently being tracked 
        /// </summary>
        public bool IsTracked 
        {
            get
            {
                return this.isTracked;
            }

            private set
            {
                if (this.IsTracked != value)
                {
                    this.isTracked = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> 
        /// Gets a value indicating whether or not the discrete gesture has been detected
        /// </summary>
        public bool Detected 
        {
            get
            {
                return this.detected;
            }

            private set
            {
                if (this.detected != value)
                {
                    this.detected = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> 
        /// Gets a float value which indicates the detector's confidence that the gesture is occurring for the associated body 
        /// </summary>
        public float Confidence
        {
            get
            {
                return this.confidence;
            }

            private set
            {
                if (this.confidence != value)
                {
                    this.confidence = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> 
        /// Gets a float value which indicates the player's magic
        /// </summary>
        public double PlayerMagic
        {
            get
            {
                return this.magicPoints;
            }

            private set
            {
                if (this.magicPoints != value)
                {
                    this.magicPoints = value;
                    if (this.magicPoints > 100)
                    {
                        this.magicPoints = 100;
                    }
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> 
        /// Gets a float value which indicates the player's health
        /// </summary>
        public double PlayerHealth
        {
            get
            {
                return this.hitPoints;
            }

            private set
            {
                if (this.hitPoints != value)
                {
                    this.hitPoints = value;
                    if (this.hitPoints > 100)
                    {
                        this.hitPoints = 100;
                    }
                    this.NotifyPropertyChanged();
                }
            }
        }

        // print the spell history
        public static void PrintValues(IEnumerable myCollection)
        {
            foreach (Object obj in myCollection)
                Console.Write("    {0}", obj);
            Console.WriteLine();
        }

        // returns true if the most recent spell matches the input spell
        public bool spellHistoryMatchArray(int [] spells)
        {
            if (this.spellHistory.Count > 0)
            {
                Array.Reverse(spells);
                int l = spells.Length;
                int i = 0;
                foreach( int spell in this.spellHistory )
                {
                    if(spell == spells[i]){
                        i++;
                        // we're at the end of the array, and they all matched
                        if (i >= l)
                        {
                            return true;
                        }
                    }
                    else{
                        return false;
                    }
                }
            }
            return false;
        }

        /// <summary> 
        /// Gets an image for display in the UI which represents the current gesture result for the associated body 
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                if (this.goldStar)
                {
                    return this.starImage;
                }
                else
                {
                    return this.kinectBodyView.imageSource;     // display the skeleton
                    //return this.imageSource;                  // display graphic image for each of the shadow weavings
                }
            }

            private set
            {
                if (this.imageSource != value)      
                {
                    this.imageSource = value;

                    if (value != this.playerNotTrackedImage)
                    {
                        // if a tracked player changes state, inform the parent game state of changes to the player's stance
                        if (value == this.playerNeutralStanceImage)
                        {
                            this.state.gestureEnds(this.bodyIndex);
                        }
                        else
                        {
                            this.state.gestureBegins(this.whichGesture, this.bodyIndex);

                            // if no previous spell, add to queue
                            if (this.spellHistory.Count == 0){ 
                                this.spellHistory.Push(this.whichGesture);
                                //PrintValues(this.spellHistory);
                            }
                            // record this spell in spell history if it is not the same as the previous spell
                            else if ((int)this.spellHistory.Peek() != this.whichGesture){
                                this.spellHistory.Push(this.whichGesture);
                                //PrintValues(this.spellHistory);
                            }
                              
                            // impose magic use penalties
                            if (this.state.minigame < 6)
                            {
                                PlayerMagic = PlayerMagic - this.magicCosts[this.whichGesture];
                            }
                        }
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        // function so that the gamestate can modify player health
        // i can be both positive and negative
        public void ChangePlayerHealth(double i)
        {
            if (this.isTracked)
            {
                this.PlayerHealth = this.PlayerHealth + i;
            }

            if (this.PlayerHealth <= 0)
            {
                this.PlayerHealth = 0;
                this.state.failMinigame();
            }
        }

        // function so that the gamestate can modify player health
        // i can be both positive and negative
        public void ChangePlayerMagic(double i)
        {
            if (this.isTracked)
            {
                this.PlayerMagic = this.PlayerMagic + i;
            }
        }

        /// <summary>
        /// Updates the values associated with the discrete gesture detection result
        /// </summary>
        /// <param name="isBodyTrackingIdValid">True, if the body associated with the GestureResultView object is still being tracked</param>
        /// <param name="isGestureDetected">True, if the discrete gesture is currently detected for the associated body</param>
        /// <param name="detectionConfidence">Confidence value for detection of the discrete gesture</param>
        public void UpdateGestureResult(bool isBodyTrackingIdValid, bool isGestureDetected, float detectionConfidence, int gesture)
        {
            PlayerMagic = PlayerMagic + this.magicPointRecovery;
            PlayerHealth = PlayerHealth + this.hitPointRecovery;

            this.IsTracked = isBodyTrackingIdValid;
            this.Confidence = 0.0f;

            if (!this.IsTracked)
            {
                this.ImageSource = this.playerNotTrackedImage;//Here!!
                this.Detected = false;
                this.BodyColor = Brushes.Gray;
            }
            else
            {
                // all the gestures interpolate between the color begin (the neutral posture color) and their own end color
                double[] begin = { 255, 255, 255 };

                this.Detected = isGestureDetected;

                if (this.Detected)
                {
                    this.Confidence = detectionConfidence;
                    this.whichGesture = gesture;

                    // make sure you have enough magic points to cast the spell
                    if (PlayerMagic > this.magicCosts[this.whichGesture])
                    {
                        if (gesture == (int) Spells.Shield)
                        {
                            this.ImageSource = this.playerShieldImage;
                            //Console.WriteLine("Shield detected");

                            double[] end = { 12, 204, 242 };
                            // linear interpolation between the begin and end rgb values, based on how high the confidence is
                            this.BodyColor = new SolidColorBrush(Color.FromArgb(255, (byte)(-(begin[0] - end[0]) * this.Confidence + begin[0]), (byte)(-(begin[1] - end[1]) * this.Confidence + begin[1]), (byte)(-(begin[2] - end[2]) * this.Confidence + begin[2])));
                        }
                        else if (gesture == (int) Spells.Attack)
                        {
                            this.ImageSource = this.playerAttackImage;
                            //Console.WriteLine("Attack detected");
                            double[] end = { 242, 2, 30 };
                            this.BodyColor = new SolidColorBrush(Color.FromArgb(255, (byte)(-(begin[0] - end[0]) * this.Confidence + begin[0]), (byte)(-(begin[1] - end[1]) * this.Confidence + begin[1]), (byte)(-(begin[2] - end[2]) * this.Confidence + begin[2])));
                        }
                        else if (gesture == (int) Spells.Bind)
                        {
                            this.ImageSource = this.playerBindImage;
                            //Console.WriteLine("Bind detected");
                            double[] end = { 39, 194, 8 };
                            this.BodyColor = new SolidColorBrush(Color.FromArgb(255, (byte)(-(begin[0] - end[0]) * this.Confidence + begin[0]), (byte)(-(begin[1] - end[1]) * this.Confidence + begin[1]), (byte)(-(begin[2] - end[2]) * this.Confidence + begin[2])));
                        }
                        else
                        {
                            this.ImageSource = this.playerRevealImage;
                            //Console.WriteLine("Reveal detected"); 
                            double[] end = { 247, 200, 10 };
                            this.BodyColor = new SolidColorBrush(Color.FromArgb(255, (byte)(-(begin[0] - end[0]) * this.Confidence + begin[0]), (byte)(-(begin[1] - end[1]) * this.Confidence + begin[1]), (byte)(-(begin[2] - end[2]) * this.Confidence + begin[2])));
                        }
                    }
                }
                else
                {
                    double[] begin2 = { 132, 135, 135 };

                    this.state.gestureEnds(this.bodyIndex);
                    this.ImageSource = this.playerNeutralStanceImage;
                    this.BodyColor = new SolidColorBrush(Color.FromArgb(255, (byte)begin2[0], (byte)begin2[1], (byte)begin2[2]));
                }
            }
        }

        /// <summary>
        /// Notifies UI that a property has changed
        /// </summary>
        /// <param name="propertyName">Name of property that has changed</param> 
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
